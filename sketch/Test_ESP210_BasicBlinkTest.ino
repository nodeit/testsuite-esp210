/*
 ESP 210 test suite
 */

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

#define LED   5

ESP8266WiFiMulti WiFiMulti;

enum testStates_e {
  STATE_DORMANT,
  STATE_CONNECTING,
  STATE_CONNECTED,
  STATE_RECONNECT,
  STATE_FINISHED,
};

volatile enum testStates_e testState = STATE_DORMANT;
volatile enum testStates_e oldState;

enum testStatus_e {
  TEST_OK,
  ERR_COULD_NOT_CONNECT,
  ERR_TEST_NOT_STARTED,
  ERR_TEST_SERVER_NO_RESPONSE,
  ERR_TEST_SERVER_RESPONSE_CODE,
  ERR_TEST_SERVER_RESPONSE_MSG,
  ERR_INCORRECT_STATE_DETECTED,
};

volatile enum testStatus_e testStatus = ERR_TEST_NOT_STARTED;
/*
 * This function changes state the state of the statemachine
 */
void gotoState(testStates_e newState)
{
  oldState = testState;
  testState = newState;
}

/*
 * Attempts to connect to a test server and retrieve some information
 */
enum testStatus_e handleConnection()
{
  HTTPClient http;
  enum testStatus_e status = TEST_OK;

  http.begin("http://www.sweetpeas.se/testdata/test.txt"); //HTTP
  
  // start connection and send HTTP header
  int httpCode = http.GET();
  
  // httpCode will be negative on error
  if(httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      // file found at server
      if(httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
          if (!payload.startsWith("Congratulations, you have successfully")) {
            status = ERR_TEST_SERVER_RESPONSE_MSG;
          }
      } else {
        status = ERR_TEST_SERVER_RESPONSE_CODE;
      }
  } else {
      status = ERR_TEST_SERVER_NO_RESPONSE;
  }
  
  http.end();

  return status;
}

// the setup function runs once when you press reset or power the board
void setup() 
{
  uint8_t MAC[6];

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  // initialize the LED pin to output.
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  // The serial channel is used to communicate with a test server on
  // the host side. Here we will receive test commands and feed back
  // test results.
  Serial.begin(115200);
  // Insert a small delay to allow the host to setup the test server
  delay(500);
  // Tell the host we are ready
  Serial.println();
  Serial.println(F("READY"));

  // Now off to do some testing
  uint32_t realSize = ESP.getFlashChipRealSize();
  uint32_t ideSize = ESP.getFlashChipSize();
  FlashMode_t ideMode = ESP.getFlashChipMode();
  WiFi.macAddress(MAC);

  Serial.printf("FLASH_REAL_ID: %08X\n", ESP.getFlashChipId());
  Serial.printf("FLASH_REAL_SIZE: %u\n", realSize);
  Serial.printf("FLASH_MODE: %s\n", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
  Serial.printf("MAC: %02x:%02x:%02x:%02x:%02x:%02x\n", MAC[0], MAC[1], MAC[2], MAC[3], MAC[4], MAC[5]);

  Serial.println("CONNECTING");
  
  WiFiMulti.addAP("FarmNet", "CountlessHours1024");

  gotoState(STATE_CONNECTING);
}

// the loop function runs over and over again forever
void loop() 
{
  static boolean networktestdone = false;
  static uint32_t startTime = millis();
  wl_status_t wifiStatus = WiFiMulti.run();

  // This is the main state machine switch
  switch (testState) 
  {
    case STATE_DORMANT:
      // Do absolutely nothing but blink =)
      digitalWrite(LED, HIGH);
      delay(100);
      digitalWrite(LED, LOW);
      delay(100);
      break;

    case STATE_FINISHED:
      if (testStatus != TEST_OK) {
        Serial.printf("TEST_FAILED: %d\n", testStatus);
      } else {
        Serial.printf("TEST_OK\n");
      }
      Serial.printf("TEST_STOPPED\n");
      Serial.flush();
      gotoState(STATE_DORMANT);
      break;

    // Wait for the device to connect to to the wlan
    case STATE_CONNECTING:
      switch(wifiStatus) 
      {
        case WL_IDLE_STATUS:
        case WL_NO_SSID_AVAIL:
          break;

        case WL_CONNECTED:
          gotoState(STATE_CONNECTED);
          break;

        case WL_CONNECT_FAILED:
          gotoState(STATE_RECONNECT);
          break;
          
        default:
          Serial.println(wifiStatus);
          delay(100);
      }
      break;

    case STATE_RECONNECT:
      // For now just go back to the connecting state and let
      // the device attempt to reconnect.
      testStatus = ERR_COULD_NOT_CONNECT;
      gotoState(STATE_FINISHED);
      
    case STATE_CONNECTED:
      Serial.printf("CONNECTED\n");
      Serial.printf("RSSI: %d\n", WiFi.RSSI());
      testStatus = handleConnection();
      gotoState(STATE_FINISHED);
      break;
      
    default:
      testStatus = ERR_INCORRECT_STATE_DETECTED;
      gotoState(STATE_FINISHED);
      break;
  }

  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);
  delay(100);
}
