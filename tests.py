#!/usr/bin/python
#
# This sequence tests the ESP210 module using the previosuly uploaded
# test firmware in the module. If all is well, basically just the MAC address
# is printed to the console to notify the user. 
# Otherwise the log wil indicate the error that occured.

import sys
import time
import serial
import string

args = len(sys.argv)
if args != 2:
  print "Invalid number of arguments."
  print "  You should have typed something like this 'tests /dev/ttyS2'"
  sys.exit()

port = sys.argv[1]
print "Using port", port

ser = serial.serial_for_url(port, do_not_open=True)
ser.baudrate = 115200
#ser.rtscts = False
#ser.xonxoff = False
ser.open()
ser.setDTR(0)
ser.setRTS(0)

line = ""
count = 0
ser.timeout = 2 # The device must respond within 2 seconds here.
while line.rstrip() != "READY":
  line = ser.readline()
  count = count + 1
  if count > 4:
    # Test failed, return error code
    sys.exit(1)

testResult = 0 # Means test okey
connectionStates = 0 # Count connections states
ser.timeout = 25 # 25 seconds for connection timeouts

while line.rstrip() != "TEST_STOPPED":
  line = ser.readline()

  # Parse response from DUT
  if line.startswith("FLASH_REAL_ID:"):
    args = line.split(" ", 2)
    if args[1].rstrip() != "001640EF":
      testResult = testResult + 1
      print "Error: Flash ID was ", args[1].rstrip(), ", Should have been 001640EF"
  elif line.startswith("FLASH_REAL_SIZE:"):
    args = line.split(" ", 2)
    if args[1].rstrip() != "4194304":
      testResult = testResult + 1
      print "Error: Flash Size incorrect. Was ", args[1].rstrip(), ", Should have been 4194304"
  elif line.startswith("FLASH_MODE:"):
    args = line.split(" ", 2)
    if args[1].rstrip() != "QIO":
      testResult = testResult + 1
      print "Error: Flash Mode incorrect. Was ", args[1].rstrip(), ", should have been QIO"
  elif line.startswith("MAC:"):
    args = line.split(" ", 2)
    print "Mac Address: ", args[1].rstrip()
  elif line.startswith("RSSI:"):
    args = line.split(" ", 2)
    rssi = int(args[1].rstrip())
    if rssi < -90:
      print "Warning: RSSI Dangerously low, was ", args[1].rstrip(), ", Should be around -70 - -90"
  elif line.startswith("CONNECTING"):
    connectionStates = 1
    print "Connecting....."
  elif line.startswith("CONNECTED"):
    connectionStates = connectionStates + 2
    print "Connected!"
  elif line.startswith("TEST_FAILED:"):
    testResult = testResult + 1
    args = line.split(" ", 2)
    print "Error: Test failed with DUT error ", args[1].rstrip()

if testResult == 0 and connectionStates == 3:
  sys.exit(0)
else:
  print "TEST FAILED, check log to see what went wrong !"

  if connectionStates == 0:
    print "I am missing both the 'CONNECTED' state and the 'CONNECTING' from the DUT"
  elif connectionStates == 1:
    print "I am missing the 'CONNECTED' state from the DUT"
  elif connectionStates == 2:
    print "I am missing the 'CONNECTING' state from the DUT"
  sys.exit(1)

